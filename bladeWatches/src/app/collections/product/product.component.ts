import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  NgZone
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  Location
} from '@angular/common';
import {
  HttpClient
} from '@angular/common/http';
import {
  Ng4LoadingSpinnerService
} from 'ng4-loading-spinner';
import {
  GlobalService
} from '../../global.service';
import {
  SwiperComponent
} from 'angular2-useful-swiper';
import 'rxjs/add/operator/map';
import {
  ShareButtons
} from '@ngx-share/core';

declare var jQuery: any;
declare var $: any;
declare var fadeInUp: any;
declare var pSlider: any;
declare var zoom: any;
declare var rViewSlider: any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements AfterViewInit, OnInit {
  @ViewChild('galleryTop') galleryTop: SwiperComponent;
  @ViewChild('galleryThumbs') galleryThumbs: SwiperComponent;
  private prod: any;
  urlParams: any;
  prodUrl: any;
  productDetail: any;
  pName: any;
  pAttributes: any;
  pImg1: any;
  pImg2: any;
  pImg3: any;
  pImg4: any;
  pImgH1: any;
  pImgH2: any;
  pImgH3: any;
  pImgH4: any;
  otherProd: any;
  galleryTopConfig: any = {
    spaceBetween: 30,
    slidesPerView: 1
  };
  galleryThumbsConfig: any = {
    nextButton: '.button-next',
    prevButton: '.button-prev',
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true
  };
  relatedView: any = {
    nextButton: '.rbutton-next',
    prevButton: '.rbutton-prev',
    spaceBetween: 30,
    slidesPerView: 4,
    breakpoints: {
      480: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      960: {
        slidesPerView: 3,
        spaceBetween: 10
      }
    }
  };

  constructor(public share: ShareButtons, private _location: Location, private route: ActivatedRoute, private router: Router, private http: HttpClient, private global: GlobalService, private spinnerService: Ng4LoadingSpinnerService, private ngZone: NgZone) {
    this.prod = this.route.params.subscribe(params => this.urlParams = params);
    this.prodUrl = this.global.baseURL + '/collections' + '/' + this.urlParams.collectionName + '/' + this.urlParams.productId;
    console.log(this.urlParams.collectionName);
    console.log(this.urlParams.productId);
  }

  getData() {
    return this.http.get(this.prodUrl);
  }
  getProdData() {

    this.getData().subscribe(data => {
      console.log(data);
      this.productDetail = data;
      this.pName = this.productDetail.prodData.name;
      this.pAttributes = this.productDetail.prodData.attributes;
      this.pImg1 = this.productDetail.prodData.image1;
      this.pImg2 = this.productDetail.prodData.image2;
      this.pImg3 = this.productDetail.prodData.image3;
      this.pImg4 = this.productDetail.prodData.image4;
      this.pImgH1 = this.productDetail.prodData.hover1;
      this.pImgH2 = this.productDetail.prodData.hover2;
      this.pImgH3 = this.productDetail.prodData.hover3;
      this.pImgH4 = this.productDetail.prodData.hover4;
      this.otherProd = this.productDetail.otherProds;
      this.spinnerService.hide();
      console.log(this.pImg3);
    })
  }

  goBack() {
    this._location.back();
  }

  viewProd(prodId) {
    window.location.reload();
    this.router.navigate(['/collections', this.urlParams.collectionName, prodId], {
      relativeTo: this.route
    });

  }

  imageZoom(imgID) {
    if (!($('#resultZoomDiv').hasClass('active'))) {
      $('#resultZoomDiv').addClass('active');
      $('.product-details').removeClass('active');
    }
    var img, lens, result, cx, cy;
    img = document.getElementById(imgID);
    result = document.getElementById('resultZoomDiv');
    /*create lens:*/
    lens = document.createElement("DIV");
    lens.setAttribute("class", "img-zoom-lens");
    /*insert lens:*/
    if ($(img.parentElement).find('.img-zoom-lens').length == 0)
      img.parentElement.insertBefore(lens, img);
    /*calculate the ratio between result DIV and lens:*/
    cx = result.offsetWidth / lens.offsetWidth;
    cy = result.offsetHeight / lens.offsetHeight;
    /*set background properties for the result DIV:*/
    result.style.backgroundImage = "url('" + img.src + "')";
    result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
    /*execute a function when someone moves the cursor over the image, or the lens:*/
    lens.addEventListener("mousemove", moveLens);
    img.addEventListener("mousemove", moveLens);
    /*and also for touch screens:*/
    lens.addEventListener("touchmove", moveLens);
    img.addEventListener("touchmove", moveLens);

    function moveLens(e) {
      var pos, x, y;
      /*prevent any other actions that may occur when moving over the image:*/
      e.preventDefault();
      /*get the cursor's x and y positions:*/
      pos = getCursorPos(e);
      /*calculate the position of the lens:*/
      // console.log(pos);
      // console.log(lens.offsetHeight);
      // console.log(lens.offsetWidth);
      x = pos.x - (lens.offsetWidth / 2);
      y = pos.y - (lens.offsetHeight / 2);
      /*prevent the lens from being positioned outside the image:*/
      if (x > img.width - lens.offsetWidth) {
        x = img.width - lens.offsetWidth;
      }
      if (x < 0) {
        x = 0;
      }
      if (y > img.height - lens.offsetHeight) {
        y = img.height - lens.offsetHeight;
      }
      if (y < 0) {
        y = 0;
      }
      /*set the position of the lens:*/
      lens.style.left = x + "px";
      lens.style.top = y + "px";
      /*display what the lens "sees":*/
      result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
    }

    function getCursorPos(e) {
      var a, x = 0,
        y = 0;
      e = e || window.event;
      /*get the x and y positions of the image:*/
      a = img.getBoundingClientRect();
      /*calculate the cursor's x and y coordinates, relative to the image:*/
      x = e.pageX - a.left;
      y = e.pageY - a.top;
      /*consider any page scrolling:*/
      x = x - window.pageXOffset;
      y = y - window.pageYOffset;
      return {
        x: x,
        y: y
      };
    }
  }

  resetZoom() {
    $('#resultZoomDiv').removeClass('active');
    $('.product-details').addClass('active');
    console.log('hide zoom');
  }


  ngOnInit() {
    this.getProdData();


    $(document).ready(function () {
      // pSlider();

      fadeInUp();
      // rViewSlider();


    });
  }
  ngAfterViewInit(): void {
    this.galleryTop.swiper.params.control = this.galleryThumbs.swiper;
    this.galleryThumbs.swiper.params.control = this.galleryTop.swiper;
    // if(this.elevatezoomBig){
    //   $(this.elevatezoomBig.nativeElement).elevateZoom({
    //       borderSize: 1,
    //       lensFadeIn: 200,
    //       cursor: 'crosshair',
    //       zoomWindowFadeIn: 200,
    //       loadingIcon: true,
    //       zoomWindowOffety: -50,
    //       zoomWindowOffetx: 50,
    //       zoomWindowHeight: 530,
    //       responsive: true
    //   });
    // }
  }

}
