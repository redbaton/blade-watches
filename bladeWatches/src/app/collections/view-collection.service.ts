import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {GlobalService} from '../global.service';
import 'rxjs/add/operator/map';

@Injectable()
export class ViewCollectionService {

	name: any;
    collectionUrl: any;
    collection: any;
    cName: any;
    cBanner: any;
    cMobBanner: any;
    productList: any;
    categories: any;
    accessUrl: any;

  	constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient, private global: GlobalService, private spinnerService: Ng4LoadingSpinnerService) {
        this.collectionUrl = global.baseURL + '/collections/';
        this.accessUrl = global.baseURL;
       }
       getData(collectionName){
		    return this.http.get(this.collectionUrl + '/' + collectionName);
	    }
	    getHomeData(col){
		
		    this.getData(col).subscribe(data => {
                this.collection = data;
                this.cBanner = this.collection.categoryData.banimage;
                this.cMobBanner = this.collection.categoryData.mbanimage;
                this.cName = this.collection.categoryData.slug;
                this.productList = this.collection.products;
                this.categories = this.collection.categories;
                this.spinnerService.hide();
		    })
	    }

  	viewCollection(collectionName): void{
      this.name = collectionName;
      this.getHomeData(this.name);
      this.router.navigate(['/collections',this.name]);
      
    }

    getAccess(cattName, accessName){
        return this.http.get(this.accessUrl + '/' + cattName + '/' + accessName);
    }

    getAccessData(catName, aes){
        this.getAccess(catName, aes).subscribe(data => {
            this.collection = data;
            this.cBanner = this.collection.categoryData.banimage;
            this.cMobBanner = this.collection.categoryData.mbanimage;
            this.cName = this.collection.categoryData.slug;
            this.productList = this.collection.products;
            this.categories = this.collection.categories;
            this.spinnerService.hide();
            // console.log(this);
        })
    }

}
