import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {ViewCollectionService} from '../collections/view-collection.service';

declare var jQuery:any;
declare var $ :any;
declare var fadeIn: any;
declare var fadeInUp: any;
declare var remFullpage: any;

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {

	id: any;
	collectionName: any;
  paramName:any;
  products:any;
  private sub: any;

  constructor(private route: ActivatedRoute, private router: Router, private spinnerService: Ng4LoadingSpinnerService, private viewcollection: ViewCollectionService) { 
    this.sub = this.route.params.subscribe( params => this.collectionName = params.collectionName  );
  }
  
  dataload(){
    this.viewcollection.viewCollection(this.collectionName);
  }

  ngOnInit() {
    this.dataload();
    remFullpage();
  	$(document).ready(function () {
      fadeIn();
      window.onscroll = function() {scrollFunction()};
      function scrollFunction() {
        if (window.pageYOffset >= 20) {
          $("#innerNav").addClass("sticky");
        } else {
          $("#innerNav").removeClass("sticky");
        }
      }
      fadeInUp();
      // $.fn.fullpage.destroy('all');

    });

  }

  Desbgstyle(){
    let DesBg = {
      'background-image': 'url(' + this.viewcollection.cBanner + ')',
      'height': '288px',
      'max-height': '288px',
      'background-position': 'center',
      'background-repeat': 'no-repeat'
    }
    return DesBg;
  }
  DesbgstyleMobile(){
    let DesBg = {
      'background-image': 'url(' + this.viewcollection.cMobBanner + ')',
      'height': '288px',
      'max-height': '288px',
      'background-position': 'center',
      'background-repeat': 'no-repeat'
    }
    return DesBg;
  }

  viewProduct(productId){
    this.id = productId;
    this.router.navigate(['/collections',this.collectionName,this.id]);
    
  }
}
