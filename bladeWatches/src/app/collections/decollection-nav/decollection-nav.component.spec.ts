import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecollectionNavComponent } from './decollection-nav.component';

describe('DecollectionNavComponent', () => {
  let component: DecollectionNavComponent;
  let fixture: ComponentFixture<DecollectionNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecollectionNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecollectionNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
