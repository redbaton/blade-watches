import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ViewCollectionService} from '../../collections/view-collection.service';
declare var jQuery:any;
declare var $ :any;

@Component({
  selector: 'app-decollection-nav',
  templateUrl: './decollection-nav.component.html',
  styleUrls: ['./decollection-nav.component.scss']
})
export class DecollectionNavComponent implements OnInit {
  selectedItem:any;
  activeParam:any;
  activeItem:any;
  activeSlug:any;

  constructor(private viewcollection: ViewCollectionService, private router: Router, private activeRoute: ActivatedRoute) {
    this.activeParam = this.activeRoute.params.subscribe( params => this.activeItem = params);
   }
  
  changeData(aName, cName){
    this.selectedItem = cName;
    this.router.navigate(['/'+aName ,cName]);
    this.viewcollection.getAccessData(aName, cName);
    this.activeSlug = cName;
  }

  ngOnInit() {
    this.selectedItem = this.activeItem.accessoriesName || this.activeItem.collectionName;
  	$(document).ready(function () {
    });
  }

}
