import {
  Component,
  OnInit,
  Renderer
} from '@angular/core';
import {
  ViewCollectionService
} from '../collections/view-collection.service';
import {
  HeaderFooterService
} from '../header-footer.service';
import {
  Router, NavigationStart
} from '@angular/router';
declare var jQuery: any;
declare var $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  _isContact = false;
  cName: any;
  headerData: any;
  constructor(private render: Renderer, private router: Router, private viewcollection: ViewCollectionService, private headerfooter: HeaderFooterService, private route: Router) {}

  ngOnInit() {
    this.headerfooter.getCommanData().subscribe(data => {
      this.headerfooter.coData = data;
      this.headerData = this.headerfooter.coData.headerData.links;
    });
    this.router.events.subscribe((val: NavigationStart) => {
      console.log(val);
      if(val.url === '/contactus')  {
        this._isContact = true;
      } else {
        this._isContact = false;
      }
    });

    $(document).ready(function () {
      $('.navbar-collapse').collapse('hide');
      $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        $('.navbar-collapse').collapse('hide');
        if (scroll >= 10) {
          $("header").addClass("scroll-header");
        } else {
          $("header").removeClass("scroll-header");
        }
      });
    });

  }

  routeCollection(name) {
    this.cName = name;
    this.viewcollection.viewCollection(this.cName);
  }



}
