import {
  Component,
  OnInit
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  GlobalService
} from '../global.service';
import {
  NgForm
} from '@angular/forms';
import 'rxjs/add/operator/map';
import { Router, NavigationStart } from '@angular/router';

declare var jQuery: any;
declare var $: any;
declare var fadeInUp: any;
declare var remFullpage: any;

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {

  contactUrl: any;

  contactData: any;
  contactPara: any;
  contactImg: any;
  countries: any;
  textString: any;
  fName: any;
  lName: any;
  company: any;
  email: any;
  city: any;
  number: any;
  country: any;
  enquirie: any;
  message: any;
  errMsg: any;
  stores: any;
  msg: any;
  submitContact: any;
  selectedCountry: any;
  caseCountry: any;
  selectCat = 'storenservice';
  base: any;
  filterStoreData: any;

  constructor(private router: Router, private http: HttpClient, private global: GlobalService, ) {
    this.contactUrl = global.baseURL + "/pages/contactus";
    this.submitContact = global.baseURL + "/enquiry";
    this.base = global.baseURL + '/storenservice';
  }
  keytab(event) {
    console.log("EVENT", event);
    if (event && event.type === "keyup") {
      const element = event.target.nextElementSibling; // get the sibling element
      // if (element === null) { // check if its null
      //   return;
      // } else {
      if(element !==null){
        element.focus(); // focus if not null
      }
    }
    if (event && event.type === "click") {
      console.log("dadasdasd");
    }
  }
  getData() {
    return this.http.get(this.contactUrl);
  }
  getContactData() {
    this.getData().subscribe(data => {
      this.contactData = data;
      this.contactPara = this.contactData.contData.contPara;
      this.contactImg = this.contactData.contData.contImg;
      this.countries = this.contactData.countries;
      this.selectedCountry = this.countries[0].country
      this.stores = this.contactData.allStores;
    })
  }

  validateString(textString) {
    if (textString.length <= 500 && textString.length >= 2) {
      return textString;
    }
  }
  validateSelect(select) {
    if (select) {
      return select;
    }
  }
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  validatePhone(phone) {
    var re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return re.test(String(phone));
  }
  onSubmit(subcribe: NgForm) {
    if (this.validateString(subcribe.value.firstName) && this.validateString(subcribe.value.lastName) && this.validateString(subcribe.value.company) && this.validateString(subcribe.value.country) && this.validateString(subcribe.value.city) && this.validateSelect(subcribe.value.enquiries) && this.validateEmail(subcribe.value.email) && this.validatePhone(subcribe.value.number)) {
      this.msg = '';
      this.fName = subcribe.value.firstName;
      this.lName = subcribe.value.lastName;
      this.company = subcribe.value.company;
      this.email = subcribe.value.email;
      this.city = subcribe.value.city;
      this.country = subcribe.value.country;
      this.number = subcribe.value.number;
      this.message = subcribe.value.message;
      this.enquirie = subcribe.value.enquiries;
      this.http.post(this.submitContact, {
        first: this.fName,
        last: this.lName,
        company: this.company,
        email: this.email,
        city: this.city,
        country: this.country,
        cont: this.number,
        type: this.enquirie,
        message: this.message
      }).subscribe((data: any) => {
        // this.msg = data.response.msg
        $('#thankyouContactModal').modal('show');
        subcribe.reset();
      });
    } else {
      if (!(this.validateString(subcribe.value.firstName))) {
        this.msg = 'Enter First Name';
      } else if (!(this.validateString(subcribe.value.lastName))) {
        this.msg = 'Enter Last Name';
      } else if (!(this.validateString(subcribe.value.company))) {
        this.msg = 'Enter Company Name';
      } else if (!(this.validateEmail(subcribe.value.email))) {
        this.msg = 'Invalid Email Address';
      } else if (!(this.validateString(subcribe.value.city))) {
        this.msg = 'Enter a City Name';
      } else if (!(this.validateString(subcribe.value.country))) {
        this.msg = 'Enter a Country Name';
      } else if (!(this.validateSelect(subcribe.value.enquiries))) {
        this.msg = 'Select an Enquiry Option'
      } else {
        this.msg = 'Invalid Contact Number';
      }
    }
  }
  ngOnInit() {
    remFullpage();
    this.getContactData();
    this.router.events.subscribe((val: NavigationStart) => {
      // console.log(val);
    });
    $(document).ready(function () {
      fadeInUp();
      $(".dropdown-menu li").click(function () {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
      });
      // console.log(window.location.pathname);
      // $.fn.fullpage.destroy('all');

    });
  }

  getfilterData() {
    return this.http.get(this.base + '/' + this.selectCat + '/' + this.selectedCountry);
  }

  filterData() {
    this.getfilterData().subscribe(data => {
      this.filterStoreData = data;
      this.stores = this.filterStoreData.allStores;
    })
  }
}
