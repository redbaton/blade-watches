import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {GlobalService} from '../global.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import 'rxjs/add/operator/map';

declare var jQuery:any;
declare var $ :any;
declare var fadeInUp: any;
declare var remFullpage: any;

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})

export class AboutusComponent implements OnInit {

  aboutUrl:any;
  aboutData:any;
  bannerHeading:any;
  bannerPara: any;
  abtBannerImg: any;
  timelineData:any;

  constructor(private http: HttpClient, private global: GlobalService, private spinnerService: Ng4LoadingSpinnerService) {
    this.aboutUrl = global.baseURL + '/pages/aboutus';
   }

  getData(){
		return this.http.get(this.aboutUrl);
	}
	getHomeData(){
		this.getData().subscribe(data => {
      this.aboutData = data;
      this.bannerHeading = this.aboutData.abtData.aboutHeading;
      this.bannerPara = this.aboutData.abtData.aboutPara;
      this.abtBannerImg = this.aboutData.abtData.abtImg;
      this.timelineData = this.aboutData.timeline;
      this.spinnerService.hide();
		})
	}

  ngOnInit() {
    this.getHomeData();

  	$(document).ready(function () {
      fadeInUp();
  		remFullpage();
  	});
  }

}
