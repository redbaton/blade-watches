import {
  Component,
  OnInit,
  NgModule
} from '@angular/core';
import {
  ViewCollectionService
} from '../collections/view-collection.service';
import {
  HeaderFooterService
} from '../header-footer.service';
import {
  GlobalService
} from '../global.service';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  share
} from "rxjs/operators";
import {
  NgForm
} from '@angular/forms';
import { Subscriber } from 'rxjs/Subscriber';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  cName: any;
  footerLinks: any;
  collectionListOne: any;
  collectionListTwo: any;
  copyright:any;
  address: any;
  call: any;
  fax: any;
  mail: any;
  sendEmail: any;
  email: any;
  msg: any;
  errMsg: any;
  popupHead: any;
  popupDes: any;
  popupImg: any;
  popupEmail: any;
  popupName: any;
  popupPhone: any;
  sendPopup: any;

  routeCollection(name) {
    this.cName = name;
    this.viewcollection.viewCollection(this.cName);
  }

  bgstyle() {
    let bg = {
      'background-image': 'url(' + this.popupImg + ')',
      'background-size': 'cover',
      'min-height': '500px',
      'background-position': 'center'
    }
    return bg;
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  validatePhone(phone) {
    var re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return re.test(String(phone));
  }
  constructor(private viewcollection: ViewCollectionService, private headerfooter: HeaderFooterService, private http: HttpClient, private global: GlobalService) {
    this.sendEmail = global.baseURL + '/newsletter';
    this.sendPopup = global.baseURL + '/offers';
  }

  onSubmit(subcribe: NgForm) {
    // let headers = new HttpHeaders({
    //   'Content-Type': 'application/json'
    //   });
    this.email = subcribe.value.subcribeEmail;
    if (this.validateEmail(this.email)) {
      // code...
      this.http.post(this.sendEmail, {
        email: this.email
      }).subscribe(
        (data: any) => {
          // this.msg = data.response.msg;
          // window.alert("Thank you for subscribing!");
          $('#thankyouModal').modal('show');
          this.msg = "";
          subcribe.reset();
        },
        error => {
          console.log(error)
        }
      );
    } else {
      this.msg = "Invalid Email Address";
    }

  }
  popupSubmit(popup: NgForm) {
    if (this.validateEmail(popup.value.email) && this.validatePhone(popup.value.phone)) {
      this.popupEmail = popup.value.email;
      this.popupName = popup.value.name;
      this.popupPhone = popup.value.phone;
      this.http.post(this.sendPopup, {
        name: this.popupName,
        cont: this.popupPhone,
        email: this.popupEmail
      }).subscribe(
        (data: any) => {
          // this.msg = data;
          $('#leadModal').modal('hide');
          $('#thankyouModal').modal('show');
          popup.reset();
        },
        error => {
          console.log(error)
        }
      );
    } else {
      if (!(this.validateEmail(popup.value.email)))
        this.errMsg = 'Invalid Email Address';
      else
        this.errMsg = 'Invalid Contact Number';
    }
  }
  ngOnInit() {
    this.headerfooter.getCommanData().subscribe(data => {
      this.footerLinks = this.headerfooter.coData.footerData.links;
      this.collectionListOne = this.headerfooter.coData.footerData.collection_list_1;
      this.collectionListTwo = this.headerfooter.coData.footerData.collection_list_2;
      this.address = this.headerfooter.coData.footerData.contactInfo.addr;
      this.call = this.headerfooter.coData.footerData.contactInfo.tel;
      this.fax = this.headerfooter.coData.footerData.contactInfo.fax;
      this.mail = this.headerfooter.coData.footerData.contactInfo.email;
      this.copyright = this.headerfooter.coData.footerData.copyright;
      this.global.fbLink = this.headerfooter.coData.footerData.sociallinks.fb;
      this.global.twLink = this.headerfooter.coData.footerData.sociallinks.twitter;
      this.global.ytLink = this.headerfooter.coData.footerData.sociallinks.youtube;
      this.global.inLink = this.headerfooter.coData.footerData.sociallinks.insta;
      this.popupHead = this.headerfooter.coData.popupData.heading;
      this.popupDes = this.headerfooter.coData.popupData.subhead;
      this.popupImg = this.headerfooter.coData.popupData.img;
    });

    $(document).ready(function () {

      let key = 'popup';
      $('#leadModal').on('hide.bs.modal', function (e) {
        sessionStorage.setItem(key, 'Open');
      })
      let val = sessionStorage.getItem(key);
      if (val) {
        $('#leadModal').modal('hide');
      } else {
        if (screen.width > 992) {
          setTimeout(() => {
            $('#leadModal').modal('show');
          }, 5000);
        }
      }
    });
  }

}

