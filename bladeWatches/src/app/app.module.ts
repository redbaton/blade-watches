import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MnFullpageModule } from "ngx-fullpage";
import { Ng4LoadingSpinnerModule,Ng4LoadingSpinnerComponent } from 'ng4-loading-spinner';
import { HttpClientModule } from '@angular/common/http';
import { SwiperModule } from 'angular2-useful-swiper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {ViewCollectionService} from './collections/view-collection.service';
import { SeoService } from './seo.service';
import { HeaderFooterService } from './header-footer.service';
import { ShareModule } from '@ngx-share/core';

import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ContactusComponent } from './contactus/contactus.component';
import { CollectionsComponent } from './collections/collections.component';
import { DecollectionNavComponent } from './collections/decollection-nav/decollection-nav.component';
import { ProductComponent } from './collections/product/product.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GlobalService } from './global.service';
import { AccessoriesComponent } from './accessories/accessories.component';
import { AccessoriesProdComponent } from './accessories/accessories-prod/accessories-prod.component';
import { ViewAccessoriesService } from './accessories/view-accessories.service';



const routes: Routes = [
  {'path':'', 'redirectTo':'/home', 'pathMatch':'full'},
  {'path':'home',
   'component':HomeComponent, 
   'data': {
      title: "Home",
      metatags: {
            description: "Page Description or some content here",
            keywords: "home"
      }
   }
 },
  {
  'path':'contactus', 
  'component':ContactusComponent,
  'data': {
        title: "Contact Us",
        metatags: {
          description: "Page Description or some content here",
          keywords: "Contact Us"
      }
    }
  },
  {
    'path':'collections/:collectionName', 
    'component':CollectionsComponent,
    'data': {
        title: "Collection",
        metatags: {
          description: "Page Description or some content here",
          keywords: "Collection"
      }
    },
    pathMatch: 'full'
  },
  {
    'path':'collections/:collectionName/:productId', 
    'component':ProductComponent,
    'data': {
        title: "Collection",
        metatags: {
          description: "Page Description or some content here",
          keywords: "Collection"
      }
    },
    pathMatch: 'full'
  },
  {
    'path':'accessories/:accessoriesName', 
    'component':AccessoriesComponent,
    'data': {
        title: "Accessories",
        metatags: {
          description: "Page Description or some content here",
          keywords: "Accessories"
      }
    },
    pathMatch: 'full'
  },
  {
    'path':'accessories/:accessoriesName/:accessoriesId', 
    'component':AccessoriesProdComponent,
    'data': {
        title: "accessories",
        metatags: {
          description: "Page Description or some content here",
          keywords: "accessories"
      }
    },
    pathMatch: 'full'
  },
  {
    'path':'aboutus', 
    'component':AboutusComponent,
    'data': {
        title: "About Us",
        metatags: {
          description: "Page Description or some content here",
          keywords: "About Us"
      }
    }
  },
  {'path':'**',
   'component':PageNotFoundComponent
 }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    ContactusComponent,
    CollectionsComponent,
    DecollectionNavComponent,
    ProductComponent,
    AboutusComponent,
    PageNotFoundComponent,
    AccessoriesComponent,
    AccessoriesProdComponent,
    
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ang4-seo-pre'}),
    RouterModule.forRoot(routes),
    HttpClientModule,
    MnFullpageModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule.forRoot()
  ],
  providers: [ViewCollectionService, SeoService, HeaderFooterService, GlobalService, ViewAccessoriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }

