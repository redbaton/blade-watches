import { TestBed, inject } from '@angular/core/testing';

import { ViewAccessoriesService } from './view-accessories.service';

describe('ViewAccessoriesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewAccessoriesService]
    });
  });

  it('should be created', inject([ViewAccessoriesService], (service: ViewAccessoriesService) => {
    expect(service).toBeTruthy();
  }));
});
