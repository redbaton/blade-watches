import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {ViewCollectionService} from '../collections/view-collection.service';


declare var jQuery:any;
declare var $ :any;
declare var fadeIn: any;
declare var fadeInUp: any;
declare var remFullpage: any;


@Component({
  selector: 'app-accessories',
  templateUrl: './accessories.component.html',
  styleUrls: ['./accessories.component.scss']
})
export class AccessoriesComponent implements OnInit {

  private sub: any;
  accessName:any;
  constructor(private route: ActivatedRoute, private router: Router, private spinnerService: Ng4LoadingSpinnerService, private viewcollection: ViewCollectionService) { 
    this.sub = this.route.params.subscribe( params => this.accessName = params.accessoriesName  );
  }

  loadAccess(){
    this.viewcollection.getAccessData('accessories',this.accessName);
  }

  Desbgstyle(){
    let DesBg = {
      'background-image': 'url(' + this.viewcollection.cBanner + ')',
      'height': '288px',
      'max-height': '288px',
      'background-position': 'center'
    }
    return DesBg;
  }
  DesbgstyleMobile(){
    let DesBg = {
      'background-image': 'url(' + this.viewcollection.cMobBanner + ')',
      'height': '288px',
      'max-height': '288px',
      'background-position': 'center'
    }
    return DesBg;
  }

  ngOnInit() {
    this.loadAccess();
    remFullpage();
    $(document).ready(function () {
      fadeIn();
      window.onscroll = function() {scrollFunction()};
      function scrollFunction() {
        if (window.pageYOffset >= 20) {
          $("#innerNav").addClass("sticky");
        } else {
          $("#innerNav").removeClass("sticky");
        }
      }
      // fadeInUp();
      // $.fn.fullpage.destroy('all');

    });
  }

}
