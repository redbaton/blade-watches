import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesProdComponent } from './accessories-prod.component';

describe('AccessoriesProdComponent', () => {
  let component: AccessoriesProdComponent;
  let fixture: ComponentFixture<AccessoriesProdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessoriesProdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessoriesProdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
