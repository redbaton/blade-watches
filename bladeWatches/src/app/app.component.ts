import { Component, NgZone } from '@angular/core';
import { Ng4LoadingSpinnerModule, Ng4LoadingSpinnerService, Ng4LoadingSpinnerComponent  } from 'ng4-loading-spinner';
import {
  Router,
  // import as RouterEvent to avoid confusion with the DOM Event
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
import {ViewCollectionService} from './collections/view-collection.service';
import { SeoService } from './seo.service';
@Component({
  selector: 'app-root',
  template: `

    <app-header></app-header>
    <ng4-loading-spinner [threshold]="2000" [template]="loadTemplate" [loadingText]="'Please wait...'" [zIndex]="9999999"> </ng4-loading-spinner>
    <router-outlet >    </router-outlet>
    <app-footer></app-footer> 

  `
})
export class AppComponent {
  title = 'app';
  
  loadTemplate: string =`<img src="http://pa1.narvii.com/5722/2c617cd9674417d272084884b61e4bb7dd5f0b15_hq.gif" />`;
  constructor(private ng4LoadingSpinnerService: Ng4LoadingSpinnerService, private router: Router, private seoService: SeoService, private ngZone: NgZone) { 
     seoService.addSeoData()
    //  router.events.subscribe((event: RouterEvent) => {
    //   this.navigationInterceptor(event)
    // })
   }

   // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.ng4LoadingSpinnerService.show();
    }
    if (event instanceof NavigationEnd) {
      this.ng4LoadingSpinnerService.hide();
    }

    // // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.ng4LoadingSpinnerService.hide();
    }
    if (event instanceof NavigationError) {
      this.ng4LoadingSpinnerService.hide();
    }
  }
   
   // this.ng4LoadingSpinnerService.show();
   // this.ng4LoadingSpinnerService.hide();
   ngOnInit() {
        this.router.events.subscribe((evt) => {
          this.navigationInterceptor(evt);
          window.scrollTo(0, 0);
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            
        });
    }
}
