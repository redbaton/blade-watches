import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {GlobalService} from './global.service';
import 'rxjs/add/operator/map';

@Injectable()
export class HeaderFooterService {

  headerUrl:any;
  coData:any;
  
  constructor(private http: HttpClient, private global: GlobalService) { 
   this.headerUrl = global.baseURL + '/common';
  }

  getCommanData(){ 
    return this.http.get(this.headerUrl);
  }
  

}
