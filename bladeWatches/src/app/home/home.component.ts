import { Component, OnInit } from '@angular/core';
import {ViewCollectionService} from '../collections/view-collection.service';
import { HttpClient } from '@angular/common/http';
import {GlobalService} from '../global.service';
import 'rxjs/add/operator/map';

declare var jQuery:any;
declare var $ :any;
declare var Swiper: any;
declare var fullpageFun: any;

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	
	cName:any;
	homeUrl:any;
	home:any;
	bannerHeadOne:any;
	bannerHeadTwo:any;
	bannerPara:any;
	bannerImg:any;
	collectionsHead:any;
	collectionsPara:any;
	accessoriesHead:any;
	accessoriesPara:any;
	socialHead:any;
	socialPara:any;
	homeCollections:any;
	homeAccessories:any;
	homeSocial:any;
	config: SwiperOptions = {
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		spaceBetween: 30,
		slidesPerView: 3,
		slidesPerGroup: 3,
		loopedSlides: 3,
		breakpoints: {
			480: {
				slidesPerView: 1,
				slidesPerGroup: 1,
				spaceBetween: 10
			},
			768: {
				slidesPerView: 2,
				slidesPerGroup: 1,
				spaceBetween: 10
			},
			960:{
				slidesPerView: 3,
				spaceBetween: 10
			}
		}
	};

	constructor(private viewcollection: ViewCollectionService, private http: HttpClient, private global: GlobalService) { 
		this.homeUrl = global.baseURL + '/pages/home';
	}
	getData(){
		
		return this.http.get(this.homeUrl);
	}
	getHomeData(){
		
		this.getData().subscribe(data => {
			this.home = data;
			this.bannerHeadOne = this.home.homeData.bannerH2;
			this.bannerHeadTwo = this.home.homeData.bannerH1;
			this.bannerPara = this.home.homeData.bannerPara;
			this.bannerImg = this.home.homeData.bannerImg;
			this.collectionsHead = this.home.homeData.collHead;
			this.collectionsPara = this.home.homeData.collPara;
			this.accessoriesHead = this.home.homeData.accesHead;
			this.accessoriesPara = this.home.homeData.accesPara;
			this.socialHead = this.home.homeData.socHead;
			this.socialPara = this.home.homeData.socPara;
			this.homeCollections = this.home.collections;
			this.homeAccessories = this.home.accessories;
			this.homeSocial = this.home.socialFeed;
			// this.spinnerService.hide();
		})
	}
	ngOnInit() {
		
		this.getHomeData();
		$(document).ready(function () {
			fullpageFun();
		});

	}

	routeCollection(name){
		this.cName = name;
		this.viewcollection.viewCollection(this.cName);
	}
}
